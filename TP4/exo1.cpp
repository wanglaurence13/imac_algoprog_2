#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    //return 0;
    return nodeIndex * 2 + 1;
}

int Heap::rightChild(int nodeIndex)
{
    //return 0;
    return nodeIndex * 2 + 2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i = heapSize;
    (*this)[i]= value;
    while(i>0 && (*this)[i] > (*this)[(i-1)/2]){
        //swap((*this)[i], (*this)[(i-1)/2]);
        swap(i, (i-1)/2);
        i = (i-1)/2;
    }

}

void Heap::heapify(int heapSize, int nodeIndex)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i_max = nodeIndex;
    int indexLeft = this->leftChild(nodeIndex);
    int indexRight = this->rightChild(nodeIndex);

    if(indexLeft < heapSize){
        if((*this)[i_max]<(*this)[indexLeft]){
            i_max = indexLeft;
        }
    }
    if(indexRight < heapSize){
        if((*this)[i_max]<(*this)[indexRight]){
            i_max = indexRight;
        }
    }
    if(i_max != nodeIndex){
        swap(nodeIndex, i_max);
        this->heapify(heapSize,i_max);
    }

}

void Heap::buildHeap(Array& numbers)
{

    int numbersL = numbers.size();
    for(int i=0; i<numbersL; i++){
        insertHeapNode(numbersL, numbers[i]);
    }
    /*int i=0;
    while(numbers[i]){
        insertHeapNode(i, numbers[i]);
    }*/

}

void Heap::heapSort()
{
    int size = this->size();
    for(int i=size-1; i>0; i-- ){
        swap(0,i);
        this->heapify(i,0);
    }

    /*for(int i=0; i>size-1; i++ ){
        swap(0,i);
        this->heapify(i,0);
    }*/
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
    w->show();

    return a.exec();
}
