#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot

    if(z.length()>2){
            return false;
        }
        else{
            if(n>0){
                Point newZ;
                newZ.x = (z.x)*(z.x) + point.x;
                newZ.y = (z.y)*(z.y) + point.y;
                return isMandelbrot(newZ, n-1, point);
            }

        }

        return true;

}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



