#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    int capacite;
    int dernier;
    // your code
};


void initialise(Liste* liste)
{
    liste->premier = NULL;
}

bool est_vide(const Liste* liste)
{
    if(liste->premier == NULL){
       return true;
    }
    return false;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud *newNoeud= new Noeud;
    if (newNoeud == NULL)
    {
        cout << "erreur dans l'allocation" << endl;
        exit(-1);
    }

    newNoeud->donnee = valeur;
    newNoeud->suivant = liste->premier;
    liste->premier = newNoeud;
}

void affiche(const Liste* liste)
{
    if (liste == NULL)
    {
       exit(-1);
    }

    Noeud *current = liste->premier;

    while (current != NULL)
    {
        cout << current->donnee << endl;
        current = current->suivant;
    }

}

int recupere(const Liste* liste, int n)
{
    if (liste == NULL)
    {
       exit(-1);
    }

    Noeud *current = liste->premier;

    for(int i=0; i<n; i++){
        current = current->suivant;
    }
    return current->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    if (liste == NULL)
    {
        exit(-1);
    }

    int index = 0;
    Noeud *current = liste->premier;
    while (current != NULL)
    {
        if(current->donnee ==valeur){
            return index;
        }
        index++;
        current = current->suivant;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    if (liste == NULL)
    {
        exit(-1);
    }

    Noeud *current = liste->premier;

    for(int i=0; i<n; i++){
        current = current->suivant;
    }
    current->donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if(tableau->dernier == tableau->capacite){

    int *newValue = new int;
    *newValue = valeur;
    tableau->donnees[tableau->dernier]=*newValue;
    tableau->capacite +=1;
    }
    else {
        tableau->donnees[tableau->dernier]=valeur;
    }

        tableau->dernier +=1;
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite = capacite;
    tableau->donnees = new int(capacite);
    tableau->dernier = 0;
}

bool est_vide(const DynaTableau* liste)
{
    if(liste->dernier == 0){
        return true;
    }
        return false;
}

void affiche(const DynaTableau* tableau)
{
    for(int i=0; i<tableau->dernier; i++){
    cout << tableau->donnees[i] << endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if(n> tableau->dernier){
        cout << "le tableau ne contient pas cette valeur";
        exit(-1);
    }
    int valeur = tableau->donnees[n];
    return valeur;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for(int i=0; i<tableau->dernier; i++){
        if(tableau->donnees[i]==valeur){
                return i;
        }
    }

    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n] = valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    Noeud *newNoeud= new Noeud;
        if (newNoeud == NULL)
        {
            cout << "erreur dans l'allocation" << endl;
            exit(-1);
        }
        newNoeud->donnee = valeur;
        newNoeud->suivant = NULL;

        if (liste->premier != NULL)
        {
            Noeud *current = liste->premier;
            while (current->suivant != NULL)
            {
                current = current->suivant;
            }
            current->suivant = newNoeud;
        }
        else
        {
            liste->premier = newNoeud;
        }
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    if (liste == NULL)
    {
        exit(-1);
    }

    int firstData = 0;

    if (liste->premier != NULL)
    {
        Noeud *toDelete = liste->premier;
        firstData = toDelete->donnee;
        liste->premier=toDelete->suivant;
        delete (toDelete);
     }

     return firstData;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud *newNoeud= new Noeud;
    if (newNoeud == NULL)
    {
        cout << "erreur dans l'allocation" << endl;
        exit(-1);
    }

        newNoeud->donnee = valeur;
        newNoeud->suivant = liste->premier;
        liste->premier = newNoeud;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    if (liste == NULL)
    {
        exit(-1);
    }

        int lastData = 0;
        Noeud *toDelete = liste->premier;

        if (liste->premier != NULL)
        {
            lastData = toDelete->donnee;
            liste->premier = toDelete->suivant;
            delete (toDelete);
        }

        return lastData;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
