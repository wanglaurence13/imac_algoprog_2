#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if(origin.size()<2){
        return;
    }

	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
    for(uint i=0; i<first.size(); i++){
        first[i]= origin[i];
    }
    for(uint i=0; i<second.size();i++){
        second[i] = origin[first.size()+i];
    }

	// recursiv splitAndMerge of lowerArray and greaterArray
    if(first.size()>1){
        splitAndMerge(first);
    }
    if(second.size()>1){
        splitAndMerge(second);
    }

	// merge
    merge(first, second, origin);

}

void merge(Array& first, Array& second, Array& result)
{
    int index_first=0;
    int index_second=0;

    for(uint i=0; i<result.size(); i++){
        if(index_first >= first.size()){
            result[i] = second[index_second];
            index_second++;
        }else if(index_second >= second.size()){

            result[i] = first[index_first];
            index_first++;

       }else if(first[index_first]<second[index_second]){
       result[i] = first[index_first];
       index_first++;
       }else{
       result[i] = second[index_second];
       index_second++;
       }
   }

}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
