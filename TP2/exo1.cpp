#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
	// selectionSort
    int min_index;
  for(uint i=0; i<toSort.size(); i++){
        min_index = i;
    for(uint j=i; j<toSort.size(); j++){
        if(toSort[j]< toSort[min_index]){
            min_index = j;
        }
    }
    toSort.swap(i, min_index);
  }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
