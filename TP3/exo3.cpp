#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    SearchTreeNode* parent;
    int value;

    void initNode(int value)
    {
        this->value = value;
        left= nullptr;
        right = nullptr;
        parent = nullptr;
    }

	void insertNumber(int value) {

        // create a new node and insert it in right or left child

              if(this->value < value){
                  if(right == nullptr){
                      right = new SearchTreeNode(value);
                  }
                  else{
                      right->insertNumber(value);
                  }
              }else{
                  if(left==nullptr){
                      left = new SearchTreeNode(value);
                  }else{
                      left->insertNumber(value);
                  }
              }


        /***balance attempt***/
        /*if(this->value < value){
            if(right == nullptr){
                right = new SearchTreeNode(value);
                right->parent = this;
                right->balance(right);

            }
            else{
                right->insertNumber(value);
            }
        }else{
            if(left==nullptr){
                left = new SearchTreeNode(value);
                left->parent = this;
                left->balance(left);
            }else{
                left->insertNumber(value);
            }
        }*/

       //SearchTreeNode* node = this;
       //node->balance(node);


    }

    int heightDifference(){
        if(this->left == nullptr && this->right == nullptr){
            return 0;
        }
        else if(this->left == nullptr){
            return this->right->height();
        }else if(this->right == nullptr){
            return this->left->height();
        }else{
            return this->right->height() - this->left->height();
        }

    }



    void balance(SearchTreeNode* node){

//        if(node->right != nullptr || node->left!=nullptr){
         printf("hello");
        int E = node->heightDifference();
        if(abs(E) > 1){
            printf("%d", E);
            if( E == -2){
//                int childEL = node->left->heightDifference();
                //int childER = heightDifference(node->right->right->height(),node->right->left->height());
                if(node->left != nullptr && node->left->heightDifference() == 1){
                   node->doubleRotateR(node);
                }else{
                     node->rotateRight(node);

                }
            }else{
//                int childER = node->right->heightDifference();
                //int childL = heightDifference(node->left->right->height(),node->left->left->height());
                if(node->right != nullptr && node->right->heightDifference() == -1){
                    node->doubleRotateL(node);
                }else{
                    node->rotateLeft(node);
                }
            }
        }else{
            if(node->parent != nullptr){
                balance(node->parent);
            }
        }
//        }

    }


	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        int height =0;
        int leftHeight=0;
        int rightHeight=0;

        if(left!=nullptr){
            leftHeight = left->height();
        }

        if(right!=nullptr){
            rightHeight= right->height();
        }

        if(rightHeight>=leftHeight){
            height=rightHeight;
        }
        if(leftHeight>rightHeight){
            height=leftHeight;
        }

        return height+1;

    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        int sum = 0;
        int countLeft = 0;
        int countRight = 0;

        if(left!=nullptr){
            countLeft = left->nodesCount();
        }

        if(right!=nullptr){
            countRight= right->nodesCount();
        }

        sum = countLeft + countRight;


        return sum+1;
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if(left==nullptr && right==nullptr){
           return true;
        }

        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        //leavesCount = 0;

        if(this->isLeaf()){
            leaves[leavesCount]=this;
            leavesCount++;
        }
        else{
            if(left!=nullptr){
                left->allLeaves(leaves, leavesCount);
            }if(right!=nullptr){
                right->allLeaves(leaves, leavesCount);
            }

        }

	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel

        if(left!=nullptr){
            left->inorderTravel(nodes,nodesCount);

        }
        //nodes[nodesCount]=left;
        //nodesCount++;
        nodes[nodesCount]=this;
        nodesCount++;

        if(right!=nullptr){
            /*nodes[nodesCount]=right;
            nodesCount++;*/
            right->inorderTravel(nodes,nodesCount);
        }

	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount]=this;
        nodesCount++;

        if(left!=nullptr){
            left->preorderTravel(nodes,nodesCount);
        }

        if(right!=nullptr){
            right->preorderTravel(nodes,nodesCount);
        }

	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel

        if(left!=nullptr){
            left->postorderTravel(nodes,nodesCount);
        }
        if(right!=nullptr){
            right->postorderTravel(nodes,nodesCount);
        }

        nodes[nodesCount]=this;
        nodesCount++;
	}

	Node* find(int value) {
        // find the node containing value
        if(this->value == value){
            return this;
        }

        if(value>this->value){
            if(right!=nullptr){
               return right->find(value);
             }
        }else{
            if(left!=nullptr){
              return  left->find(value);
            }
        }

        return nullptr;

	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    void rotateLeft(SearchTreeNode*& node){


        SearchTreeNode* tmpParent = node->parent;
        SearchTreeNode* tmp = node->right;
        node->right = node->right->left;
        tmp->left = node;
        if(node == node->parent->right){
            node->parent->right = tmp;
        }
        node->parent = tmp;
        if(node->right != nullptr){
            node->right->parent = node;
        }
        tmp->parent = tmpParent;


        /*SearchTreeNode* tmp = node->right;
        node->right = node->left;
        tmp->left = node;
        node = tmp;*/
        /*SearchTreeNode* tmp = this->right;
        this->right = this->left;
        tmp->left = this;
        this = tmp;*/

        /*if(node->right !=nullptr){
        SearchTreeNode* tmpParent = node->parent;
        SearchTreeNode* tmp = node->right;



        if(node->right!=nullptr){
            node->right = node->right->left;
            node->right->parent = node;
        }


        tmp->left = node;
        node->parent=tmp;

        tmp->parent = tmpParent;
        node = tmp;
        }*/




    }

    void rotateRight(SearchTreeNode*& node){

        /*if(node->left!=nullptr){

        SearchTreeNode* tmpParent = node->parent;
        SearchTreeNode* tmp = node->left;


        if(node->left !=nullptr){
            node->left = node->left->right;
            node->left->parent = node;
        }

        tmp->right = node;
        node->parent = tmp;

        tmp->parent = tmpParent;
        node = tmp;

        }*/

        SearchTreeNode* tmpParent = node->parent;
        SearchTreeNode* tmp = node->left;
        node->left = node->left->right;
        tmp->right = node;
        if(node == node->parent->left){
            node->parent->left = tmp;
        }
        node->parent = tmp;
        if(node->left != nullptr){
            node->left->parent = node;
        }
        tmp->parent = tmpParent;

        /*SearchTreeNode* tmp = node->left;
        node->left = node->right;
        tmp->right = node;
        node = tmp;*/

        /*SearchTreeNode* tmp = this->left->left;
        this->right = this->left;
        this->left = tmp;*/

        /*SearchTreeNode* tmp = this->left;
        this->left = this->right;
        tmp->right = this;
        this = tmp;*/
    }

    void doubleRotateL(SearchTreeNode*& node){
        rotateRight(node->right);
        rotateLeft(node);

    }

    void doubleRotateR(SearchTreeNode*& node){
        rotateLeft(node->left);
        rotateRight(node);

    }



    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
